# OpenML dataset: Towards-Data-Science-articles-dataset-(2010-2021)

https://www.openml.org/d/43375

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Towards Data Science Inc. is a corporation registered in Canada as a platform for thousands of people to exchange ideas and to expand understanding of data science.
This dataset contains data for the Towards Data Science article since 2010. It will be updated on a monthly basis. The data in this dataset was extracted from https://towardsdatascience.com/archive
Content
As of now, the dataset consists of 40 133 articles and 8 features:

Publication date
Title
Author
Article URL
Number of claps
Number of responses
Amount of time spent on reading based on the average reading speed of an adult (roughly 265 WPM).
Paid or free (Participation in Medium Partner Program)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43375) of an [OpenML dataset](https://www.openml.org/d/43375). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43375/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43375/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43375/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

